
function economicalBowlers(matches, deliveries, year) {

    const ids = matches
        .filter(match => match.season == year)
        .map(match => parseInt(match.id))

    const totalDeliveries = deliveries
        .filter(delivery => ids.includes(parseInt(delivery['match_id'])));

    // console.log(totalDeliveries)
    let totalRuns = {}, overs = {}, result = {}
    totalRuns = totalDeliveries.reduce((accu, delivery) => {
        const runs = delivery['total_runs']
        const bowler = delivery.bowler

        if (accu.hasOwnProperty(bowler)) {
            accu[bowler] += parseInt(runs);
            if (parseInt(delivery.ball) == 6) {
                overs[bowler] += 1
            }
        }
        else {
            accu[bowler] = parseInt(runs);
            overs[bowler] = 0
        }
        return accu
    }, {})

    // console.log(totalRuns)

    for (let index in totalRuns) {
        result[index] = Number((totalRuns[index] / overs[index]).toPrecision(3))
    }

    const bestBowlers = Object.fromEntries(
        Object.entries(result).sort(([, a], [, b]) => a - b).slice(0, 10)
    )
    console.log(bestBowlers)
    return bestBowlers
}

module.exports = economicalBowlers