


function matchesWonPerTeamPerYear(matches) {
    let res = {}
    res = matches.reduce( (accu,match) => {
        let season = match.season
        if (!accu.hasOwnProperty(season)) {
            accu[season] = {}
        }
        return accu
    }, {})
    res = matches.reduce((accu, match) => {
        let season = match.season

        if (accu.hasOwnProperty(season)) {
            let winner = match.winner

            if (accu[season].hasOwnProperty(winner)) {
                accu[season][winner] += 1
            } else {
                accu[season][winner] = 1
            }
        }else{
            accu[season] = {}
        }
        return accu
    }, {})
    // console.log(res)
    return res

}

module.exports = matchesWonPerTeamPerYear