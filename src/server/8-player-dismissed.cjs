function playerDismissed(deliveries) {
  const res = deliveries.reduce((acc, curr) => {
    if (!curr.player_dismissed) {                    // Return blank into acc when there is no player dismissed   
      return acc
    }

    if (acc.hasOwnProperty(curr.player_dismissed)) {      // Returns info of every batsman dismissed by every bowler

      if (acc[curr.player_dismissed][curr.bowler]) {
        acc[curr.player_dismissed][curr.bowler]++
      } else {
        acc[curr.player_dismissed][curr.bowler] = 1
      }
    } else {
      acc[curr.player_dismissed] = { [curr.bowler]: 1 }
    }
    return acc
  }, {})

  let playerDismissal = Object.entries(res).map((ele) => {      // Internally sorts for every batsman according to dismissals by bowler
    ele[1] = Object.entries(ele[1]).sort(

      (playerA, playerB) => playerB[1] - playerA[1]
    )
    let score = ele[1][0]                                       // Stores the most dismissals for a batsman

    return [ele[0], score].flat()
  })

  playerDismissal = playerDismissal             // Finally sorts batsmen according to most dismissals
    .map((value) => value)
    .sort((a, b) => {

      aOut = a[2]
      bOut = b[2]
      if (aOut > bOut) {
        return -1
      } else if (aOut < bOut) {
        return 1
      } else {
        return 0
      }
    })

  let prop = `${playerDismissal[0][0]} by ${playerDismissal[0][1]}`       // Key: X by Y (after retrieving best result) 
  let value = playerDismissal[0][2]                                       // Value : no of dismissal on a pair of batsman :  bowler
  let result = {}

  result[prop] = value          // Stores the best dismissals into key : value pair

  return result
}


module.exports = playerDismissed 