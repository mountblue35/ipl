
function extraRunsPerTeam(matches, deliveries, year) {

    const ids = matches
        .filter(match => match.season == year)
        .map(match => parseInt(match.id))

    const deliveriesLog = deliveries
        .filter(delivery => ids.includes(parseInt(delivery['match_id'])));

    let result = {}

    result = deliveriesLog.reduce((accu, delivery) => {
        const extras = delivery['extra_runs']
        const team = delivery.bowling_team

        if (accu.hasOwnProperty(team)) {
            accu[team] += parseInt(extras)
        }
        else {
            accu[team] = parseInt(extras)
        }
        return accu
    }, {})

    console.log(result)
    return result;

}

module.exports = extraRunsPerTeam