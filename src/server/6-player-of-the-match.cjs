
function playerOfTheMatch(matches) {

    let result = {}, seasonal = {}
    seasonal = matches.reduce((accu, match) => {

        let year = match.season
        let pom = match.player_of_match

        if (accu.hasOwnProperty(year)) {
            if (accu[year].hasOwnProperty(pom)) {
                accu[year][pom]++
            } else {
                accu[year][pom] = 1
            }
        } else {
            accu[year] = {}
            accu[year][pom] = 1
        }
        return accu
    }, {})

    let sortable = {}
    sortable = Object.entries(seasonal).map(season => {
        let players = Object.entries(season[1])

        return [season[0], (players.sort(([, a], [, b]) => b - a))]
    })

    result = sortable.map(season => {

        let obj = {}
        obj[season[0]] = season[1][0][0]

        return obj
    })

    return result


}

module.exports = playerOfTheMatch