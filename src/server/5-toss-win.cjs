
function tossWin(matches) {
    let res = {}

    res = matches.reduce((accu, match) => {
        let tossWinner = match.toss_winner
        let matchWinner = match.winner

        if (tossWinner === matchWinner) {

            if (accu.hasOwnProperty(matchWinner)) {
                accu[matchWinner]++
            }
            else {
                accu[matchWinner] = 1
            }
        }
        return accu

    }, {})

    return res

}

module.exports = tossWin