function bestEconomyBowler(deliveries) {

    let superOver = deliveries.filter(curr => {
        let isSuperOver = curr.is_super_over
        if (isSuperOver == 1) {
            return true
        } else {
            return false
        }
    })
    let result = superOver.reduce((acc, curr) => {
        let bowler = curr.bowler

        if (acc.hasOwnProperty(bowler)) {
            acc[bowler][0] += +curr.total_runs

            if (!(curr.noball_runs > 0 || curr.wide_runs > 0)) {
                acc[bowler][1]++
            }

        } else {
            if (!(curr.noball_runs > 0 || curr.wide_runs > 0)) {
                acc[bowler] = [+curr.total_runs, 1]
            } else {
                acc[bowler] = [+curr.total_runs, 0]

            }
        }
        return acc

    }, {})

    let bestBowler = Object.entries(result).sort((a, b) => {
        economyA = a[1][0] / (a[1][1] / 6)
        economyB = b[1][0] / (b[1][1] / 6)

        if (economyA > economyB) {
            return 1
        } else if (economyA < economyB) {
            return -1
        }
    })

    return bestBowler[0][0]
}


module.exports = bestEconomyBowler