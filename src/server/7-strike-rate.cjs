
function strikeRate(matches, deliveries) {
    let myMap = new Map()
    matches
        .forEach((match) => myMap.set(match.id, match.season))      // Contains id : season info

    const strikeRateLog = deliveries            // Returns runs and balls info in an arr for every batsman every year into strikeRateLog
        .reduce((accu, curr) => {
            let year = myMap.get(curr.match_id)
            if (accu.hasOwnProperty(year)) {

                if (accu[year].hasOwnProperty(curr.batsman)) {
                    accu[year][curr.batsman][0] += +curr.batsman_runs
                    accu[year][curr.batsman][1]++
                } else {
                    accu[year][curr.batsman] = [+curr.batsman_runs, 1]
                }
            } else {
                accu[year] = { [curr.batsman]: [+curr.batsman_runs, 1] }
            }

            return accu;

        }, {})

    let result = Object.entries(strikeRateLog)          // Returns strike rate of every batsman in every year into result in [] form
        .map((striker) =>
            [striker[0],
            Object.entries(striker[1])
                .map((strike) => [strike[0], (strike[1][0] * 100) / strike[1][1]])
            ].flat())

    result = result.reduce((acc, curr) => {         // Returns into result, object notation of each season's every batsman strike rate ifno
        let key = curr[0]
        let value = curr.slice(1)

        value = value.reduce((acc2, curr2) => {
            let key2 = curr2[0]
            let value2 = curr2[1]

            acc2[key2] = value2

            return acc2
        }, {})

        acc[key] = value

        return acc
    }, {})

    return result
}

module.exports = strikeRate