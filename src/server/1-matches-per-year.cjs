
function matchesPerYear(matches) {

    let res = {}
    res = matches.reduce((accu, match) => {
        let season = match.season
        
        if (accu.hasOwnProperty(season)) {

            accu[season]++
        }
        else {
            accu[season] = 1
        }
        return accu

    }, {})

    return res

}

module.exports = matchesPerYear