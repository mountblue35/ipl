const path = require("path");

const csvMatchesPath = path.join(__dirname, "src/data/matches.csv");
const csvDeliveriesPath = path.join(__dirname, "src/data/deliveries.csv");
const csv = require("csvtojson");
const fs = require("fs");


const matchesPerYear = require(path.join(__dirname, './src/server/1-matches-per-year.cjs'))
csv()
    .fromFile(csvMatchesPath)
    .then((matches) => {
        let result = matchesPerYear(matches)

        fs.writeFileSync(path.join(__dirname, './src/public/output/1-matches-per-year.json'), JSON.stringify(result), "utf-8")
    })

const matchesWonPerTeamPerYear = require(path.join(__dirname, './src/server/2-matches-won-per-team-per-year.cjs'))
csv()
    .fromFile(csvMatchesPath)
    .then((matches) => {
        let result = matchesWonPerTeamPerYear(matches)
        console.log()

        fs.writeFileSync(path.join(__dirname, './src/public/output/2-matches-won-per-team-per-year.json'), JSON.stringify(result), "utf-8")
    })

const extraRunsPerTeam = require(path.join(__dirname, './src/server/3-extra-runs-per-team.cjs'))
csv()
    .fromFile(csvMatchesPath)
    .then((matches) => {
        csv()
            .fromFile(csvDeliveriesPath)
            .then((deliveries) => {

                let result = extraRunsPerTeam(matches, deliveries, 2016)

                fs.writeFileSync(path.join(__dirname, './src/public/output/3-extra-runs-per-team.json'), JSON.stringify(result), "utf-8")
            })
    })


const economicalBowlers = require(path.join(__dirname, './src/server/4-economical-bowlers.cjs'))
csv()
    .fromFile(csvMatchesPath)
    .then((matches) => {
        csv()
            .fromFile(csvDeliveriesPath)
            .then((deliveries) => {

                let result = economicalBowlers(matches, deliveries, 2015)

                fs.writeFileSync(path.join(__dirname, './src/public/output/4-economical-bowlers.json'), JSON.stringify(result), "utf-8")
            })
    })

const tossWin = require(path.join(__dirname, './src/server/5-toss-win.cjs'))
csv()
    .fromFile(csvMatchesPath)
    .then((matches) => {
        let result = tossWin(matches)

        fs.writeFileSync(path.join(__dirname, './src/public/output/5-toss-win.json'), JSON.stringify(result), "utf-8")
    })

const playerOfTheMatch = require(path.join(__dirname, './src/server/6-player-of-the-match.cjs'))
csv()
    .fromFile(csvMatchesPath)
    .then((matches) => {

        let result = playerOfTheMatch(matches)

        fs.writeFileSync(path.join(__dirname, './src/public/output/6-player-of-the-match.json'), JSON.stringify(result), "utf-8")
    })

const strikeRate = require(path.join(__dirname, './src/server/7-strike-rate.cjs'))
csv()
    .fromFile(csvMatchesPath)
    .then((matches) => {
        csv()
            .fromFile(csvDeliveriesPath)
            .then((deliveries) => {

                let result = strikeRate(matches, deliveries)

                fs.writeFileSync(path.join(__dirname, './src/public/output/7-strike-rate.json'), JSON.stringify(result), "utf-8")
            })
    })

const playerDismissed = require(path.join(__dirname, './src/server/8-player-dismissed.cjs'))
csv()
    .fromFile(csvDeliveriesPath)
    .then((deliveries) => {
        let result = playerDismissed(deliveries)
        fs.writeFileSync(path.join(__dirname,
            './src/public/output/8-player-dismissed.json'),
            JSON.stringify(result),
            "utf-8"
        )
    })

const bestEconomyBowler = require(path.join(__dirname, './src/server/9-best-economy-bowler.cjs'))
csv()
    .fromFile(csvDeliveriesPath)
    .then((deliveries) => {

        let result = bestEconomyBowler(deliveries)

        fs.writeFileSync(path.join(__dirname,
            './src/public/output/9-best-economy-bowler.json'),
            JSON.stringify(result),
            "utf-8"
        )
    })
